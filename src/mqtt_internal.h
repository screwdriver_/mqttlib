#ifndef MQTT_INTERNAL_H
# define MQTT_INTERNAL_H

# include <mqttlib.h>
# include <stdint.h>

# define MQTT_LEN_MAX	65535

# ifndef NO_ASSERT
#  include <stdlib.h>
#  define assert(x)\
	do\
	{\
		if(!(x))\
			abort();\
	}\
	while(0)
# else
#  define assert(x)
# endif

typedef enum
{
	RESERVED	= 0,
	CONNECT		= 1,
	CONNACK		= 2,
	PUBLISH		= 3,
	PUBACK		= 4,
	PUBREC		= 5,
	PUBREL		= 6,
	PUBCOMP		= 7,
	SUBSCRIBE	= 8,
	SUBACK		= 9,
	UNSUBSCRIBE	= 10,
	UNSUBSACK	= 11,
	PINGREQ		= 12,
	PINGRESP	= 13,
	DISCONNECT	= 14,
	AUTH		= 15
} mqtt_ctrl_packet_type_t;

typedef struct
{
	mqtt_ctrl_packet_type_t ctrl_packet_type:4;
	int mqtt_flags:4;
	uint8_t remaining_length;
} mqtt_fixed_hdr_t;

typedef uint16_t mqtt_len_t;

mqtt_len_t mqtt_read_string(mqtt_ctx_t *ctx, char *buff, mqtt_len_t len);
void mqtt_write_string(mqtt_ctx_t *ctx, const char *buff);

int64_t mqtt_read_variable_integer(mqtt_ctx_t *ctx);
void mqtt_write_variable_integer(mqtt_ctx_t *ctx, uint64_t i);

mqtt_len_t mqtt_read_binary(mqtt_ctx_t *ctx, char *buff,
	mqtt_len_t len);
void mqtt_write_binary(mqtt_ctx_t *ctx, const char *buff, mqtt_len_t len);

// TODO Read and write packet functions

int mqtt_check_fixed_hdr(const mqtt_fixed_hdr_t *hdr);

#endif
