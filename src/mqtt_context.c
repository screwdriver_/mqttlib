#include <mqttlib.h>
#include <mqtt_internal.h>

void mqtt_init(mqtt_ctx_t *ctx)
{
	assert(ctx);
	ctx->read_buffer = malloc(MQTT_BUFFER_SIZE); 
	ctx->read_buffer_cursor = NULL;
	assert(ctx->read_buffer);
	ctx->err = 0;
	ctx->reason = 0;
}

void mqtt_destroy(mqtt_ctx_t *ctx)
{
	assert(ctx);
	free(ctx->read_buffer); 
}

static size_t read_full(mqtt_ctx_t *ctx, char *buff, size_t len)
{
	assert(ctx && buff);
	ctx->err = 0;
	// TODO
	(void) len;
	return 0;
}

static size_t write_full(mqtt_ctx_t *ctx, const char *buff, size_t len)
{
	int l;
	size_t i = 0;

	assert(ctx && buff);
	ctx->err = 0;
	while(i < len
		&& (l = ctx->write_hook(ctx->sock_ctx, buff + i, len - i)) > 0)
	{
		if(l < 0)
		{
			ctx->err = 1;
			return 0;
		}
		if(i >= len)
			return i;
		i += l;
	}
	return i;
}

mqtt_len_t mqtt_read_string(mqtt_ctx_t *ctx, char *buff, mqtt_len_t len)
{
	mqtt_len_t l;

	assert(ctx && buff);
	ctx->err = 0;
	if(read_full(ctx, (char *) &l, sizeof(l)) != sizeof(l))
		goto err;
	l = MIN(l, len);
	if(read_full(ctx, buff, l) != l)
		goto err;
	return l;

err:
	ctx->err = 1;
	return 0;
}

void mqtt_write_string(mqtt_ctx_t *ctx, const char *buff)
{
	mqtt_len_t l;

	assert(ctx && buff);
	ctx->err = 0;
	l = strnlen(buff, MQTT_LEN_MAX);
	if(write_full(ctx, (char *) &l, sizeof(l)) != sizeof(l)
		|| write_full(ctx, buff, l) != l)
	{
		ctx->err = 1;
	}
}

int64_t mqtt_read_variable_integer(mqtt_ctx_t *ctx)
{
	int64_t multiplier = 1;
	int64_t value = 0;
	int8_t encoded_byte;

	assert(ctx);
	ctx->err = 0;
	do
	{
		if((ctx->read_hook(ctx->sock_ctx, (char *) &encoded_byte,
			sizeof(encoded_byte))) <= 0)
		{
			ctx->err = 1;
			return 0;
		}
		value += (encoded_byte & 127) * multiplier;
		if(multiplier > 128 * 128 * 128)
		{
			ctx->err = 1;
			return 0;
		}
		multiplier *= 128;
	}
	while((encoded_byte & 128) != 0);
	return value;
}

void mqtt_write_variable_integer(mqtt_ctx_t *ctx, uint64_t i)
{
	int8_t encoded_byte;

	do
	{
		encoded_byte = i % 128;
		i /= 128;
		if(i > 0)
			encoded_byte |= 128;
		if((ctx->write_hook(ctx->sock_ctx, (const char *) &encoded_byte,
			sizeof(encoded_byte))) <= 0)
		{
			ctx->err = 1;
			return;
		}
	}
	while(i > 0);
}

mqtt_len_t mqtt_read_binary(mqtt_ctx_t *ctx, char *buff,
	mqtt_len_t len)
{
	mqtt_len_t l;

	assert(ctx && buff);
	ctx->err = 0;
	if(read_full(ctx, (char *) &l, sizeof(l)) != sizeof(l))
		goto err;
	l = MIN(l, len);
	if(read_full(ctx, buff, l) != l)
		goto err;
	return l;

err:
	ctx->err = 1;
	return 0;
}

void mqtt_write_binary(mqtt_ctx_t *ctx, const char *buff, mqtt_len_t len)
{
	assert(ctx && buff);
	ctx->err = 0;
	len = MIN(len, MQTT_LEN_MAX);
	if(write_full(ctx, (char *) &len, sizeof(len)) != sizeof(len)
		|| write_full(ctx, buff, len) != len)
	{
		ctx->err = 1;
	}

}

int mqtt_check_fixed_hdr(const mqtt_fixed_hdr_t *hdr)
{
	// TODO
	(void) hdr;
	return 1;
}
