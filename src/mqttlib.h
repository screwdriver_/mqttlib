#ifndef MQTTLIB_H
# define MQTTLIB_H

# include <string.h>

# define MQTT_FLAG_BROKER	0b1

# define MQTT_BUFFER_SIZE	131072

# define MIN(x, y)	((x) < (y) ? (x) : (y))

typedef enum
{
	SUCCESS									= 0x00,
	NORMAL_DISCONNECTION					= 0x00,
	GRANTED_QOS_0							= 0x00,
	GRANTED_QOS_1							= 0x01,
	GRANTED_QOS_2							= 0x02,
	DISCONNECT_WITH_WILL_MESSAGE			= 0x04,
	NO_MATCHING_SUBSCRIBERS					= 0x10,
	NO_SUBSCRIPTION_EXISTED					= 0x11,
	CONTINUE_AUTHENTICATION					= 0x18,
	REAUTHENTICATE							= 0x19,
	UNSPECIFIED_ERROR						= 0x80,
	MALFORMED_PACKET						= 0x81,
	PROTOCOL_ERROR							= 0x82,
	IMPLEMENTATION_SPECIFIC_ERROR			= 0x83,
	UNSUPPORTED_PROTOCOL_VERSION			= 0x84,
	CLIENT_IDENTIFIER_NOT_VALID				= 0x85,
	BAD_USER_NAME_OR_PASSWORD				= 0x86,
	NOT_AUTHORIZED							= 0x87,
	SERVER_UNAVAILABLE						= 0x88,
	SERVER_BUSY								= 0x89,
	BANNED									= 0x8a,
	SERVER_SHUTTING_DOWN					= 0x8b,
	BAD_AUTHENTICATION_METHOD				= 0x8c,
	KEEP_ALIVE_TIMEOUT						= 0x8d,
	SESSION_TAKEN_OVER						= 0x8e,
	TOPIC_FILTER_INVALID					= 0x8f,
	TOPIC_NAME_INVALID						= 0x90,
	PACKET_IDENTIFIER_IN_USE				= 0x91,
	PACKET_IDENTIFIER_NOT_FOUND				= 0x92,
	RECEIVE_MAXIMUM_EXCEEDED				= 0x93,
	TOPIC_ALIAS_INVALID						= 0x94,
	PACKET_TOO_LARGE						= 0x95,
	MESSAGE_RATE_TOO_HIGH					= 0x96,
	QUOTA_EXCEEDED							= 0x97,
	ADMINISTRATIVE_ACTION					= 0x98,
	PAYLOAD_FORMAT_INVALID					= 0x99,
	RETAIN_NOT_SUPPORTED					= 0x9a,
	QOS_NOT_SUPPORTED						= 0x9b,
	USE_ANOTHER_SERVER						= 0x9c,
	SERVER_MOVED							= 0x9d,
	SHARED_SUBSCRIPTIONS_NOT_SUPPORTED		= 0x9e,
	CONNECTION_RATE_EXCEEDED				= 0x9f,
	MAXIMUM_CONNECT_TIME					= 0xa0,
	SUBSCRIPTION_IDENTIFIERS_NOT_SUPPORTED	= 0xa1,
	WILDCARD_SUBSCRIPTIONS_NOT_SUPPORTED	= 0xa2
} mqtt_reason_t;

typedef struct
{
	int flags;

	void *sock_ctx;
	int (*read_hook)(void *, char *, size_t);
	int (*ready_hook)(void *);
	int (*write_hook)(void *, const char *, size_t);

	char *read_buffer, *read_buffer_cursor;

	int err;
	mqtt_reason_t reason;
} mqtt_ctx_t;

void mqtt_init(mqtt_ctx_t *ctx);
void mqtt_destroy(mqtt_ctx_t *ctx);

#endif
